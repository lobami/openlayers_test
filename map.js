  var baseMapLayer = new ol.layer.Tile({
    source: new ol.source.OSM()
  });
  var map = new ol.Map({
    target: 'map',
    layers: [ baseMapLayer],
    view: new ol.View({
            center: ol.proj.fromLonLat([-100.38893879999999,20.6165067]), 
            zoom: 18 //Initial Zoom Level
          })
  });
  //Adding a marker on the map
  var marker = new ol.Feature({
    geometry: new ol.geom.Point(
      ol.proj.fromLonLat([-100.38893879999999,20.6165067])
    ),  // oficina
  });
  var vectorSource = new ol.source.Vector({
    features: [marker]
  });
  var markerVectorLayer = new ol.layer.Vector({
    source: vectorSource,
  });
  map.addLayer(markerVectorLayer);

  //add image on the map


var north = 20.6165067;
var south = 20.6165067;
var east = -100.38893879999999;
var west = -100.38893879999999;
var extent = ol.proj.transformExtent([east, north, west, south], 'EPSG:4326', 'EPSG:3857');
var imageLayer = new ol.layer.Image({
  source: new ol.source.ImageStatic({
                url: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/d6bb7f2f-45e5-42c9-a220-71fee095e12e/d72iov3-a040f8cf-eef7-429a-8a20-6fb716dc650c.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2Q2YmI3ZjJmLTQ1ZTUtNDJjOS1hMjIwLTcxZmVlMDk1ZTEyZVwvZDcyaW92My1hMDQwZjhjZi1lZWY3LTQyOWEtOGEyMC02ZmI3MTZkYzY1MGMucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.JSILZRW2nDFkyWD25131hDNl477BzJ98sp91YOS-T0c',
                imageExtent: extent
              })
});
map.addLayer(imageLayer);